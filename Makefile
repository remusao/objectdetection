
all:
	@make -C src/

training:
	@make -C src/ training

detection:
	@make -C src/ detection

clean:
	@rm -fvr main
