#! /usr/bin/python2

from operator import itemgetter

import pickle
import cv2
import numpy as np
from sys import argv
from sklearn.naive_bayes import GaussianNB


#windowName = 'test'
#cv2.namedWindow(windowName)


def overlapRectangles(rectangles):
    """
        Given a list of rectangles, merge the ones
        that overlap with others
    """

    def overlap1D(x0, w0, x1, w1):
        if x0 > x1:
            x0, x1 = x1, x0
            w0, w1 = w1, w0
        return (x1 <= (x0 + w0))


    def overlap(r1, r2):
        x1, y1, w1, h1 = r1
        x2, y2, w2, h2 = r2

        return overlap1D(x1, w1, x2, w2) and overlap1D(y1, h1, y2, h2)

    def compare(a, b):
        if a[0] == b[0]:
            return a[1] < b[1]
        return a[0] < b[0]

    if len(rectangles) == 0:
        return []

    # Sort rectangles by X, then by Y
    rectangles.sort(compare)

    # Init the resulting list
    res = [rectangles.pop(0)]

    for rect1 in rectangles:

        toMerge = [rect for rect in res if overlap(rect1, rect)]

        # Merge rectangles
        if toMerge:
            # Delete rectangles from res list
            # Merge rectangle
            points = []
            toMerge.append(rect1)

            for rect in toMerge:
                # Remove from res if it exists
                try:
                    res.remove(rect)
                except ValueError:
                    pass
                x, y, w, h = rect
                points.append((x, y))
                points.append((x + w, y + h))

            # Get max and min X
            points.sort(key=itemgetter(0))
            minX = points[0][0]
            maxX = points[-1][0]

            # Get max and min Y
            points.sort(key=itemgetter(1))
            minY = points[0][1]
            maxY = points[-1][1]

            # Compute new bounding box
            res.append((minX, minY, maxX - minX, maxY - minY))

        else: # Add the rectangle in list
            res.append(rect1)

    return res



def getBoundingBoxes(image):
    """
    """

    # Detect external contours contours
    contours, hierarchy = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    boxes = []
    for rect in overlapRectangles(overlapRectangles([cv2.boundingRect(c) for c in contours if len(c) >= 20])):
        boxes.append(rect)

    return boxes


def threshold(image):
    """
        Take an image in HSV domain and filter
        red colors. The resulting image is a binary
        one where white pixels are red on the original
        image and vis versa.
    """

    # Separate compoments
    h = image[:, :, 0]
    s = image[:, :, 1]
    v = image[:, :, 2]

    # Select red pixels
    red = (h <= 10) | (h >= 176)
    red &= (s > 120)
    red &= (v > 100)

    # Not red is set to black
    image[~red] = 0, 0, 0
    # Red is set to white
    image[red] = 0, 0, 255

    # Convert back to RGB domain
    binary = cv2.cvtColor(image, cv2.COLOR_HSV2BGR)

    # Debug purpose
    cv2.imwrite('binaire.jpg', binary)

    return binary


def detect(img, classifier):

    # 1. Convert it to HSV and filter red colors
    binary = threshold(cv2.cvtColor(img, cv2.COLOR_BGR2HSV))

    # 2. Detect bounding box arround objets in the image
    blured = cv2.cvtColor(binary , cv2.COLOR_BGR2GRAY)
    bbox = getBoundingBoxes(blured)

    # 3. Compute features of the objects detected

    # Create a new image for each
    for i, rect in enumerate(bbox):
        # Detect bounding rect
        x0, y0, w, h = rect

        # Create new image arround the item
        image = blured[y0:y0 + h, x0:x0 + w]

        # Detect full contour
        contours, hierarchy = cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        # Get points of the contour
        if len(contours) > 0:

            points = np.concatenate(contours)
            cv2.drawContours(image, contours, -1, 255, 2)
            cv2.imwrite('img%i.jpg' % i, image)

            # Compute Hu Moments
            # Create array of moments for training the classifier
            moments = cv2.HuMoments(cv2.moments(points))
            array_moment = np.zeros((1, 7), np.double)
            for j, v in enumerate(moments):
                array_moment[0, j] = v
            array_moment = -np.sign(array_moment) * np.log10(np.absolute(array_moment))
            print array_moment
            print classifier.predict(array_moment)
            if classifier.predict(array_moment)[0] == 1:
                print 'found'
                # Add rectangle on the original image
                cv2.rectangle(img, (x0, y0), (x0 + w, y0 + h), 255, 6)

    # Output original image with bounding boxes
    cv2.imwrite('img.jpg', img)



def main(images):


    gnb = None
    with open('gnb.dump', 'r') as f:
        gnb = pickle.load(f)

    # Compute features for every images
    for p in images:
        img = cv2.imread(p)
        detect(img, gnb)


if __name__ == '__main__':
    if len(argv) > 1:
        main(argv[1:])
