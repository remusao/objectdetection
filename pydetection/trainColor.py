#! /usr/bin/python2

from sklearn import mixture
import cv2
import numpy as np
import matplotlib.pyplot as plt

from sys import argv


def main(img):

    bornes = np.zeros((3, 256), np.int32)
    colors = set([])

    for i, p in enumerate(img):
        image = cv2.imread(p)
        hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

        # Separate compoments
        h = hsv[:, :, 0]
        s = hsv[:, :, 1]
        v = hsv[:, :, 2]

        # Select red pixels
        red = (h >= 170) | (h <= 10)
        red &= (s > 100)
        red &= (v > 100)

        # out = np.copy(image)

        for pixel in image[red]:
            h, s, v = pixel
            bornes[0, h] += 1
            bornes[1, s] += 1
            bornes[2, v] += 1
            colors.add((h, s, v))
        g = mixture.GMM(n_components=2)
        #for i in xrange(0, 3):
        #    amin = np.amin(image[red, i])
        #    amax = np.amax(image[red, i])
#
#            bornes[(i, 0)] = min(bornes[(i, 0)], amin)
#           bornes[(i, 1)] = max(bornes[(i, 1)], amax)



    plt.plot(range(0, 256), bornes[0, :])
    plt.show()
    plt.plot(range(0, 256), bornes[2, :])
    plt.show()
    plt.plot(range(0, 256), bornes[1, :])
    plt.show()

if __name__ == '__main__':
    main(argv[1::])
