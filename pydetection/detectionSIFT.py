#! /usr/bin/python2

from operator import itemgetter

import cv2
import numpy as np
from sys import argv

def overlapRectangles(rectangles):
    """
        Given a list of rectangles, merge the ones
        that overlap with others
    """

    def overlap1D(x0, w0, x1, w1):
        if x0 > x1:
            x0, x1 = x1, x0
            w0, w1 = w1, w0
        return (x1 <= (x0 + w0))


    def overlap(r1, r2):
        x1, y1, w1, h1 = r1
        x2, y2, w2, h2 = r2

        return overlap1D(x1, w1, x2, w2) and overlap1D(y1, h1, y2, h2)

    def compare(a, b):
        if a[0] == b[0]:
            return a[1] < b[1]
        return a[0] < b[0]

    if len(rectangles) == 0:
        return []

    # Sort rectangles by X, then by Y
    rectangles.sort(compare)

    # Init the resulting list
    res = [rectangles.pop(0)]

    for rect1 in rectangles:

        toMerge = [rect for rect in res if overlap(rect1, rect)]

        # Merge rectangles
        if toMerge:
            # Delete rectangles from res list
            # Merge rectangle
            points = []
            toMerge.append(rect1)

            for rect in toMerge:
                # Remove from res if it exists
                try:
                    res.remove(rect)
                except ValueError:
                    pass
                x, y, w, h = rect
                points.append((x, y))
                points.append((x + w, y + h))

            # Get max and min X
            points.sort(key=itemgetter(0))
            minX = points[0][0]
            maxX = points[-1][0]

            # Get max and min Y
            points.sort(key=itemgetter(1))
            minY = points[0][1]
            maxY = points[-1][1]

            # Compute new bounding box
            res.append((minX, minY, maxX - minX, maxY - minY))

        else: # Add the rectangle in list
            res.append(rect1)

    return res



def getBoundingBoxes(image):
    """
    """

    # Detect external contours contours
    contours, hierarchy = cv2.findContours(image, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    boxes = []
    for rect in overlapRectangles(overlapRectangles([cv2.boundingRect(c) for c in contours if len(c) >= 20])):
        boxes.append(rect)

    return boxes


def threshold(image):
    """
        Take an image in HSV domain and filter
        red colors. The resulting image is a binary
        one where white pixels are red on the original
        image and vis versa.
    """

    # Separate compoments
    h = image[:, :, 0]
    s = image[:, :, 1]
    v = image[:, :, 2]

    # Select red pixels
    red = (h <= 10) | (h >= 176)
    red &= (s > 120)
    red &= (v > 100)

    # Not red is set to black
    image[~red] = 0, 0, 0
    # Red is set to white
    image[red] = 0, 0, 255

    # Convert back to RGB domain
    binary = cv2.cvtColor(image, cv2.COLOR_HSV2BGR)

    # Debug purpose
    cv2.imwrite('binaire.jpg', binary)

    return binary


def detect(img, fextractor, matcher, desc, kp, template):

    # 1. Convert it to HSV and filter red colors
    binary = threshold(cv2.cvtColor(img, cv2.COLOR_BGR2HSV))

    # 2. Detect bounding box arround objets in the image
    blured = cv2.cvtColor(binary , cv2.COLOR_BGR2GRAY)
    bbox = getBoundingBoxes(blured.copy())

    # 3. Compute features of the objects detected

    # Create a new image for each
    maxImage = None
    maxRect = None
    maxMatch = 0
    for i, rect in enumerate(bbox):
        # Detect bounding rect
        x0, y0, w, h = rect

        # Create new image arround the item
        image = blured[y0:y0 + h, x0:x0 + w]

        # Feature extraction
        kp1, desc1 = fextractor.detectAndCompute(image, None)

        # No descriptors found
        if desc1 is None:
            continue

        try:
            matches = matcher.knnMatch(desc, desc1, k = 2)
        except:
            matches = []

        # Select good matches
        good = []
        for m, n in matches:
            if m.distance < 0.7 * n.distance:
                good.append(m)

        if len(good) > maxMatch:
            maxMatch = len(good)
            maxRect = rect
            maxImage = image.copy()

    if maxRect is not None:
        print maxMatch
        x0, y0, w, h = maxRect
        cv2.rectangle(img, (x0, y0), (x0 + w, y0 + h), 255, 6)
        cv2.imwrite('img.jpg', img)



def main(images):

    # Low Hessian threshold to compute lot of keypoints
    fextractor = cv2.SIFT()

    # Compute descriptors from template
    template = cv2.imread('template.jpg')
    template = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
    keys, desc = fextractor.detectAndCompute(template, None)

    # Matcher
    FLANN_INDEX_KDTREE = 0
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)

    flann = cv2.FlannBasedMatcher(index_params, search_params)

    # Compute features for every images
    for p in images:
        img = cv2.imread(p)
        detect(img, fextractor, flann, desc, keys, template)


if __name__ == '__main__':
    if len(argv) > 1:
        main(argv[1:])
