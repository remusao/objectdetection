#ifndef DESCRIPTOR_EXTRACTION_HH_
# define DESCRIPTOR_EXTRACTION_HH_

// SIFT in openCV
# include <opencv2/legacy/legacy.hpp>
# include <opencv2/objdetect/objdetect.hpp>
# include <opencv2/nonfree/nonfree.hpp>
# include <opencv2/nonfree/features2d.hpp>

// OpenCV
# include <opencv/highgui.h>
# include <opencv/cv.h>

# include <vector>

# include "utils.hh"
/*
#include "scaleSpace.hh"
#include "differenceOfGaussians.hh"
#include "featureSelection.hh"
*/

void extractDescriptors(
    const cv::Mat& image,
    cv::Mat& descriptors,
    std::vector<cv::KeyPoint>& keypoints,
    const char* window_name = nullptr);

#endif /* !DESCRIPTOR_EXTRACTION_HH_ */
