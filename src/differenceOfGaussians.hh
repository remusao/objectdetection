#ifndef DIFFERENCE_OF_GAUSSIANS_HH_
# define DIFFERENCE_OF_GAUSSIANS_HH_

# include <vector>
# include <opencv/cv.h>

std::vector<std::vector<cv::Mat>>
laplacianOfGaussian(const std::vector<std::vector<cv::Mat>>& sspace);

#endif /* !DIFFERENCE_OF_GAUSSIANS_HH_ */
