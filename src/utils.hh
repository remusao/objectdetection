#ifndef UTILS_HH_
# define UTILS_HH_

# include <iostream>

namespace utils
{
# ifdef DEBUG
    inline void trace(const char* msg)
    {
        std::cerr << msg << std::endl;
    }
# else
    inline void trace(const char*)
    {
    }
# endif
}

#endif /* !UTILS_HH_ */
