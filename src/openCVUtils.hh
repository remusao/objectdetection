#ifndef OPEN_CV_UTILS_HH_
# define OPEN_CV_UTILS_HH_

# include <memory>
# include <opencv/cv.h>

using Image = std::unique_ptr<IplImage>;

struct IplImageDeleter
{
    void operator()(IplImage** img)
    {
        cvReleaseImage(img);
    }
};


Image createImage(IplImage* img)
{
    return Image(img, IplImageDeleter());
}

#endif /* !OPEN_CV_UTILS_HH_ */
