#include "featureSelection.hh"

using namespace cv;

template <typename Op>
bool isPeak(int i, int x, int y, const std::vector<Mat>& loG, Op comp)
{
    bool res = true;
    double e = loG[i].at<double>(x, y);

    for (int j = 0; j < loG.size(); ++j)
    {
        if (j != i)
        {
            res &= comp(e, loG[j].at<double>(x, y));
        }
    }

    for (const auto& m: loG)
    {
        res &= (comp(e, m.at<double>(x + 1, y))
            &&  comp(e, m.at<double>(x + 1, y + 1))
            &&  comp(e, m.at<double>(x + 1, y - 1))
            &&  comp(e, m.at<double>(x - 1, y))
            &&  comp(e, m.at<double>(x - 1, y + 1))
            &&  comp(e, m.at<double>(x - 1, y - 1))
            &&  comp(e, m.at<double>(x, y + 1))
            &&  comp(e, m.at<double>(x, y - 1)));
    }

    return res;
}


std::vector<std::vector<PeakPoint>>
findPeakPoints(const std::vector<std::vector<cv::Mat>>& loG)
{
    std::vector<std::vector<PeakPoint>> res(loG.size(), std::vector<PeakPoint>());

    for (unsigned o = 0; o < loG.size(); ++o)
    {
        const auto& octave = loG[o];

        // Find Peak Points
        for (unsigned index = 0; index < octave.size(); ++index)
        {
            for (int x = 1; x < (octave[index].cols - 1); ++x)
            {
                for (int y = 1; y < (octave[index].rows - 1); ++y)
                {
                    if (isPeak(index, x, y, octave, [](double a, double b) { return a > b; })
                        || isPeak(index, x, y, octave, [](double a, double b) { return a < b; }))
                    {
                        res[o].push_back(PeakPoint{x, y, index});
                    }
                }
            }
        }
    }

    return res;
}
