#include <iostream>
#include <algorithm>
#include "descriptorExtraction.hh"


int main(int argc, char* argv[])
{
    using namespace cv;

    const char* window_name = "Object detector training";

    // Create window
    namedWindow(window_name, CV_WINDOW_AUTOSIZE);

    if (argc < 2)
    {
        std::cout << argv[0] << " image [image]" << std::endl;
        return -1;
    }

    // Keypoints and descriptors
    std::vector<KeyPoint> keypoints;
    Mat descriptors;

    for (int i = 1; i < argc; ++i)
    {
        // Reading image
        auto image = imread(argv[i], 0);

        if (image.empty())
        {
            std::cerr << argv[i] << " could not be loaded..." << std::endl;
            continue;
        }

        // Feature extraction
        Mat tmp_desc;
        std::vector<KeyPoint> tmp_keypoints;
        extractDescriptors(image, tmp_desc, tmp_keypoints, window_name);

        descriptors.push_back(tmp_desc);

        std::copy(tmp_keypoints.begin(), tmp_keypoints.end(), std::back_inserter(keypoints));
    }

    // Dump descriptors
    utils::trace("Dump descriptors");
    imwrite("descriptors.jpg", descriptors);

    // Dump keypoints
    utils::trace("Dump keypoints");
    FileStorage fs("keypoints.dump", FileStorage::WRITE);
    write(fs, "keypoints", keypoints);

    // Release memory
    destroyAllWindows();

    return 0;
}
