#ifndef SCALE_SPACE_HH_
# define SCALE_SPACE_HH_

# include <vector>
# include <opencv/cv.h>
# include "openCVUtils.hh"


struct ScaleSpaceOptions
{
    float       sigma;
    float       k;
    unsigned    octaves;
    unsigned    steps;
};

std::vector<std::vector<Image>>
buildScaleSpace(const cv::Mat& m, const ScaleSpaceOptions& opt);

#endif /* !SCALE_SPACE_HH_ */
