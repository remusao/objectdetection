#include "scaleSpace.hh"

using namespace cv;

// Compute scale space
std::vector<std::vector<Image>>
buildScaleSpace(Image& m, const ScaleSpaceOptions& opt)
{
    std::vector<std::vector<Image>> sspace(opt.octaves, std::vector<Image>(opt.steps, createImage(nullptr)));

    // Augment the number of stable key points by bluring the original image
    // and resizing x2
    cvSmooth(m.get(), m.get(), CV_GAUSSIAN, 0, 0, 0.5);

    Mat src = m, dst;

    // For each octave
    for (unsigned i = 0; i < opt.octaves; ++i)
    {
        float gaussParam = opt.sigma;

        // For each k compute the blured version
        for (unsigned j = 0; j < opt.steps; ++j)
        {
            GaussianBlur(src, dst, Size(0, 0), gaussParam, gaussParam);

            sspace[i][j] = dst.clone();
            gaussParam *= opt.k;
        }

        // Subsample the image
        // TODO : Check if we must subsample original or 2 * sigma picture
        pyrDown(src, dst, Size(src.cols / 2, src.rows / 2));
        src = dst.clone();
    }

    return sspace;
}
