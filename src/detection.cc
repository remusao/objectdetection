#include "descriptorExtraction.hh"

int main(int argc, char* argv[])
{
    using namespace cv;

    const char* window_name = "Object detection";

    // Create window
    namedWindow(window_name, CV_WINDOW_AUTOSIZE);

    if (argc != 2)
    {
        std::cout << argv[0] << " image" << std::endl;
        return -1;
    }

    // Load reference image
    Mat can = imread("can2.jpg", CV_LOAD_IMAGE_GRAYSCALE);

    // Reading image
    utils::trace("Reading image");
    auto image = imread(argv[1], CV_LOAD_IMAGE_GRAYSCALE);

    if (image.empty() || can.empty())
    {
        utils::trace("Image could not be read...");
        return -1;
    }

/*
    // Reading descriptors
    utils::trace("Reading descriptors");
    auto descriptors = imread(argv[2]);

    if (descriptors.empty())
    {
        utils::trace("Descriptors could not be read...");
        return -1;
    }

    // Reading keypoints
    utils::trace("Reading keypoints");
    FileStorage fs(argv[3], FileStorage::READ);
    FileNode n = fs["keypoints"];
    std::vector<KeyPoint> keypoints;
    read(n, keypoints);
*/
    // Matching descriptors
    utils::trace("Extracting descriptors from reference image");
    Mat descriptors;
    std::vector<KeyPoint> keypoints;
    extractDescriptors(can, descriptors, keypoints, window_name);

    utils::trace("Extracting descriptors from query image");
    Mat image_descriptors;
    std::vector<KeyPoint> image_keypoints;
    extractDescriptors(image, image_descriptors, image_keypoints, window_name);

    utils::trace("Matching images");
    FlannBasedMatcher matcher;
    std::vector<DMatch> matches;
    matcher.match(image_descriptors, descriptors, matches);

    double max_dist = 0;
    double min_dist = 100;

    //-- Quick calculation of max and min distances between keypoints
    for(int i = 0; i < descriptors.rows; ++i)
    {
        double dist = matches[i].distance;
        if (dist < min_dist)
        {
            min_dist = dist;
        }
        if (dist > max_dist)
        {
            max_dist = dist;
        }
    }

    std::cout << "-- Max dist : " << max_dist << std::endl;
    std::cout << "-- Min dist : " << min_dist << std::endl;

    //-- Draw only "good" matches (i.e. whose distance is less than 2*min_dist )
    //-- PS.- radiusMatch can also be used here.
    std::vector<DMatch> good_matches;

    for (int i = 0; i < descriptors.rows; ++i)
    {
        if (matches[i].distance <= 2 * min_dist)
        {
            good_matches.push_back(matches[i]);
        }
    }

    //-- Draw only "good" matches
    Mat img_matches;
    drawMatches(can, keypoints, image, image_keypoints, good_matches, img_matches, Scalar::all(-1), Scalar::all(-1), std::vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

    //-- Show detected matches
    imshow(window_name, img_matches);
    waitKey();

    // Displaying matches found
    std::cout << "Matches found: " << good_matches.size() << std::endl;

    imwrite("output.jpg", img_matches);

    return 0;
}
