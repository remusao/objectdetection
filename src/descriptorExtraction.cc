#include "descriptorExtraction.hh"


void extractDescriptors(
    const cv::Mat& image,
    cv::Mat& descriptors,
    std::vector<cv::KeyPoint>& keypoints,
    const char* window_name)
{
    using namespace cv;

    // Debug - Show original image
    imshow(window_name, image);
    waitKey();

    // SIFT features
    SiftFeatureDetector detector(10000);

    utils::trace("Extracting features");

    // Extract features
    detector.detect(image, keypoints);

    utils::trace("Drawing features");

    // Draw keypoints
    cv::Mat output;
    drawKeypoints(image, keypoints, output);
    imshow(window_name, output);
    waitKey();

    utils::trace("Creating SIFT descriptors");

    // SIFT descriptors
    SiftDescriptorExtractor extractor;
    extractor.compute(image, keypoints, descriptors);

/*
    auto sspace = buildScaleSpace(image, ScaleSpaceOptions{1.6f, 1.4f, 3, 4});

    // -------------------------------------------------------------------------
    //                                                      Debug purpose only |
    // -------------------------------------------------------------------------
    for (const auto& octave: sspace)                                        // |
    {                                                                       // |
        for (const auto& m: octave)                                         // |
        {                                                                   // |
            cvShowImage(window_name, m);                                    // |
            waitKey();                                                      // |
        }                                                                   // |
    }                                                                       // |
    // -------------------------------------------------------------------------


    auto doG = laplacianOfGaussian(sspace);

    // -------------------------------------------------------------------------
    //                                                      Debug purpose only |
    // -------------------------------------------------------------------------
    for (const auto& octave: doG)                                           // |
    {                                                                       // |
        for (const auto& m: octave)                                         // |
        {                                                                   // |
            cvShowImage(window_name, m);                                    // |
            waitKey();                                                      // |
        }                                                                   // |
    }                                                                       // |
    // -------------------------------------------------------------------------


    auto peaks = findPeakPoints(doG);

    // -------------------------------------------------------------------------
    //                                                      Debug purpose only |
    // -------------------------------------------------------------------------
    for (const auto& peak: peaks[0])                                        // |
    {                                                                       // |
        circle(                                                             // |
            image,                                                          // |
            Point{peak.x, peak.y},                                          // |
            peak.sigma * 5 + 1,                                             // |
            CV_RGB(255, 100, 0));                                           // |
    }                                                                       // |
    // -------------------------------------------------------------------------

    cvShowImage(window_name, src);
    waitKey();
*/
}
