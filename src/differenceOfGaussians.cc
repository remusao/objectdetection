#include "differenceOfGaussians.hh"


std::vector<std::vector<cv::Mat>>
laplacianOfGaussian(const std::vector<std::vector<cv::Mat>>& sspace)
{
    using namespace cv;

    std::vector<std::vector<Mat>> doG(sspace.size(), std::vector<Mat>());

    for (unsigned o = 0; o < sspace.size(); ++o)
    {
        const auto& octave = sspace[o];
        doG[o].resize(octave.size() - 1, Mat());

        // Compute the doG by substraction of gaussians
        for (unsigned j = 1; j < octave.size(); ++j)
        {
            doG[o][j - 1] = octave[j] - octave[j - 1];
        }
    }

    return doG;
}
