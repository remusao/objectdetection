#ifndef FEATURE_SELECTION_HH_
# define FEATURE_SELECTION_HH_

#include <vector>
#include <opencv/cv.h>

struct PeakPoint
{
    int         x;
    int         y;
    unsigned    sigma;
};

std::vector<std::vector<PeakPoint>>
findPeakPoints(const std::vector<std::vector<cv::Mat>>& loG);

#endif /* !FEATURE_SELECTION_HH_ */
