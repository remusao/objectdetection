\documentclass[11pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[francais]{babel}
\usepackage[T1]{fontenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}

\title{Traitement d'image}
\author{Rémi Berson \\ Vincent Latrouite}
\date{}

\begin{document}

\maketitle
\tableofcontents

\newpage

\section{Introduction}

\paragraph{}
Ce document a pour but de présenter les détails d'implémentation
de notre projet de traitement d'image. De nombreuses pistes ont été
explorées au cours de nos travaux, et elles seront étudiées une à une
au long de ce rapport.

\paragraph{Sujet}
Le sujet traité est la \emph{détection de canettes de coca-cola} dans une image donnée.



\begin{figure}[hbtp]
\centering
\includegraphics[width=\textwidth]{original.jpg}
\caption{Image originale dans laquelle détecter une canette}
\label{image_originale}
\end{figure}


\newpage
\section{Pré-traitement}


\paragraph{}
La phase de pré-traitement a pour objectif de modifier l'image donnée en
entrée afin d'améliorer les résultats de la reconnaissance, de simplifier
la suite du processus de détection, d'éliminer les informations inutiles,
etc. Notre phase de pré-traitement est composé des étapes suivantes :
\begin{enumerate}
	\item Passage en domaine \textsc{hsv} pour filtrer la couleur rouge~;
	\item \textit{blob detection} afin de créer des \textit{bounding box} autour des objets.
\end{enumerate}


\subsection{Filtrage de la couleur rouge}

\paragraph{}
Afin d'éliminer un maximum d'information inutile dans l'image, et prenant en compte
que nous recherchons des canettes de coca-cola dans nos images, il parait judicieux
d'éliminer un maximum d'information inutile en filtrant les couleurs pour ne garder
que celles susceptibles de se trouver sur une canette.

\paragraph{\textsc{hsv}}
Pour ce faire nous avons décidé
de passer d'un domaine \textsc{rgb} (Rouge Vert Bleu) à un domaine \textsc{hsv}
\textit{(Hue Saturation Value)}. Ce dernier permet de filtrer plus efficacement les couleurs
puisque leur valeur est encodée sur l'unique canal \emph{Hue}\footnote{Les autres canaux
\textit{Saturation} et \textit{Value} représentant respectivement la saturation et
l'intensité de la couleur en question}. Il est ainsi plus simple de filtrer les couleurs,
indépendamment de la luminosité, etc.

\paragraph{Autres représentations}
Néanmoins, il est à noter que puisque nous ne sommes
pas intéressés par toutes les teintes de rouge, il sera nécessaire de filtrer les canaux
\textit{Saturation} et \textit{Value} également. Dans ces conditions on peut se demander
si passer en domaine \textsc{hsv} a vraiment un intérêt, cela serait possible également
en \textsc{rgb} par exemple. Les pics sont peut-être un peu plus marqués sur les histogrammes
\textsc{hsv} que sur ceux de \textsc{rgb}.


\paragraph{}
Afin de sélectionner les valeurs à conserver lors du filtrage, deux possibilités s'offrent à
nous :
\begin{enumerate}
	\item fixer les seuils de façon arbitraire, en regardant l'histogramme des couleurs
des canettes et en choisissant de conserver les pics~;
	\item utiliser un apprentissage automatique avec un \textsc{gmm} \textit{(Gaussian Mixture
	Model)} par exemple.
\end{enumerate}


\paragraph{Histogramme}
Afin de choisir les intervalles de valeurs à conserver lors de la sélection
du rouge, on peut visualiser l'histogramme [\ref{fig:histogrammes}] et décider
de ne garder que les pics sur chacun des histogrammes. En pratique on remarque
que pour le canal \textit{Hue}, les valeurs à conserver sont dans l'intervalle
$ [0;10] \cup [175;180]$\footnote{Dans \textit{OpenCV} les valeurs de la
composante \textit{Hue} sont comprises dans l'intervalle $[0;180]$}.


\begin{figure}
        \centering                
        \begin{subfigure}[b]{0.8\textwidth}
                \includegraphics[width=\textwidth]{h.png}
                \caption{Histogramme du canal \textit{Hue}}
        \end{subfigure}
        
        \begin{subfigure}[b]{0.4\textwidth}
                \includegraphics[width=\textwidth]{s.png}
                \caption{Histogramme du canal \textit{Saturation}}
        \end{subfigure}%
        ~
        \begin{subfigure}[b]{0.4\textwidth}
                \includegraphics[width=\textwidth]{v.png}
                \caption{Histogramme du canal \textit{Value}}
        \end{subfigure}
        \caption{Histogrammes des canaux \textsc{hsv} sur un échantillon de canettes
        de coca-cola}
        \label{fig:histogrammes}
\end{figure}


\paragraph{\textit{Gaussian Mixture Model}}
L'idée ici est de laisser apprendre automatiquement à un \textsc{gmm} le modèle
des couleurs. Comme on le voit sur les histogrammes, nous sommes en présence de
plusieurs pics. Un \textsc{gmm} considèrerait que chaque pic est une gaussienne,
et nous pourrions alors utiliser la probabilité d'appartenance d'une couleur au
modèle pour déterminer si un pixel donné appartient à une canette ou non.



\subsection{Élimination du bruit}

\paragraph{}
Dans l'éventualité où le filtrage des couleurs rouges produirait des images bruitées,
comme c'est le cas lorsque les seuils de filtrage des couleurs sont mal fixés
[\ref{image_binaire_bruit}], il serait nécessaire de diminuer le bruit de l'image
afin de ne laisser que les objets intéressants dans l'image binaire. \emph{Cette
étape n'est plus nécessaire à l'heure actuelle puisque nos seuils de filtrage des
couleurs sont correctement fixés.}


\begin{figure}[hbtp]
\centering
\includegraphics[width=0.8\textwidth]{binaire.jpg}
\caption{Image binaire bruitée}
\label{image_binaire_bruit}
\end{figure}

\paragraph{Érosion - dilatation}
Afin de faire disparaitre les petits regroupements de pixels blancs, une première idée
est d'effectuer une ou plusieurs érosions - dilatation. Cela permet d'obtenir une image
moins bruitée [\ref{image_erode_dilate}].

\begin{figure}[hbtp]
\centering
\includegraphics[width=0.8\textwidth]{erode_dilate.jpg}
\caption{Image après une érosion-dilatation}
\label{image_erode_dilate}
\end{figure}

\paragraph{}
Une autre possibilité est d'appliquer un flou médian sur l'image. Ce qui va avoir pour effet
d'éliminer les pixels isolés dans une zone quasi-homogène [\ref{image_flou_median}].

\begin{figure}[hbtp]
\centering
\includegraphics[width=0.8\textwidth]{flou_median.jpg}
\caption{Image après un flou médian}
\label{image_flou_median}
\end{figure}




\subsection{Segmentation de l'image binaire}

\paragraph{}
L'étape de détection des composantes de l'image binaire a plusieurs objectifs.
Premièrement elle permet d'éliminer le bruit éventuellement présent (en filtrant
les objets trop petits). Deuxièmement, elle permet de créer une image représentant
proprement les zones d'intérêt de l'image, dans lesquelles nous pourrons chercher
l'objet à détecter".

\paragraph{}
Pour remplir ces objectifs il suffit d'utiliser un algorithme de \textit{blob detection}
afin d'isoler les différentes composantes de l'image, on obtient les résultats illustrés
sur la figure [\ref{fig:segmentation}]. Il est également possible de détecter les contours
des différents objets dans l'image, et de créer des \textit{bounding box} autour de ces
contours.


\begin{figure}
        \centering
        \begin{subfigure}[b]{0.8\textwidth}
                \includegraphics[width=\textwidth]{detection.jpg}
                \caption{\textit{Bounding box} autour des objets d'intérêt}
        \end{subfigure}
                        
        \begin{subfigure}[b]{0.25\textwidth}
                \includegraphics[width=\textwidth, height=110pt]{img0.jpg}
                \caption{Contours de la canette}
        \end{subfigure}
        ~
        \begin{subfigure}[b]{0.25\textwidth}
                \includegraphics[width=\textwidth, height=110pt]{img1.jpg}
                \caption{Contours du bouchon}
        \end{subfigure}%
        ~
        \begin{subfigure}[b]{0.25\textwidth}
                \includegraphics[width=\textwidth, height=110pt]{img2.jpg}
                \caption{Contours de l'étiquette de la bouteille}
        \end{subfigure}
        \caption{Détection des objets rouges dans l'image}
        \label{fig:segmentation}
\end{figure}



\subsection{Bilan du pré-traitement}


\paragraph{}
Durant le projet, plusieurs pistes ont été envisagées puis abandonnées, au profit de
solutions plus efficaces ou intéressantes. En voici la liste :
\begin{enumerate}
	\item passage en domaine \textsc{hsv}~;
	\item filtrage de la couleur rouge grâce à des seuils déterminées avec
	les histogrammes~;
	\item détection des contours~;
	\item création de \textit{bounding box} autour des contours.
\end{enumerate}

\paragraph{}
On remarque donc que le pré-traitement nous a permis de cibler assez précisément
les éléments susceptibles d'être une canette de coca-cola. Sur l'image prise en exemple,
on remarque qu'au terme du processus, seuls trois objets persistent :
\begin{itemize}
	\item l'étiquette de la bouteille~;
	\item la canette~;
	\item le bouchon
\end{itemize}
Les étapes suivantes vont donc permettre de discriminer plus précisément les différents
objets restants dans l'image.




\newpage
\section{Détection de la canette}

\paragraph{}
Après avoir effectué le pré-traitement sur notre image, nous disposons
d'un certain nombre d'objets identifiés, pouvant être des canettes de
coca-cola. La suite du travail consiste donc à discriminer ces objets
afin de déterminer si une canette se trouve bien dans l'image.

\subsection{Invariants de Hu}

\subsubsection{Principe}

\paragraph{Moment}
En mathématiques, un moment est une valeur représentant une caractéristique d'un nuage de
points. On peut calculer autant de moments que l'on désire. On peut donc dire qu'un ensemble
de moments permet de caractériser un nuage de points d'une certaine manière.

\paragraph{Invariant de Hu}
En traitement d'image, on peut utiliser la caractérisation en moment, qui permet d'obtenir
des valeurs représentant certaines caractéristiques d'une image. Notamment, M. K. Hu proposa
sept valeurs invariantes permettant de caractériser une image. Ces moments sont invariants
à la rotation, à l'échelle et à la translation.

\paragraph{}
L'idée est donc de calculer les sept moments de Hu sur une image \textit{template}, puis de
comparer ces moments aux moments de tous les objets présents dans une image afin de savoir
si il s'agit d'une canette ou non.

\subsubsection{Matching}

\paragraph{}
Afin de déterminer si les moments entre deux images correspondent, plusieurs techniques ont été
expérimentées. Avant toute chose, il est nécessaire d'effectuer une transformation sur les moments
afin de les rendre utilisables. Soit $m$ un moment, effectuons la transformation suivante :
\begin{equation*}
	m = -\text{sign}(m) * \text{log}_{10}(\text{abs}(m))
\end{equation*}

\paragraph{}
Les méthodes suivantes ont été expérimentées afin de discriminer les images en fonction de
leurs invariants de Hu :
\begin{itemize}
	\item Simple différence puis comparaison avec un seuil fixé~;
	\item apprentissage par un \textsc{gmm} \textit{(Gaussian Mixture Model)}~;
	\item apprentissage par un \textsc{svm} \textit{(Support Vector Machine)}~;
	\item classifieur Bayésien naïf.
\end{itemize}

\paragraph{}
Les résultats n'étaient pas très concluants.


\subsection{\textit{Scale Invariant Feature Transform}}


\subsubsection{Principe}

\paragraph{Descripteur}
Un descripteur est un ensemble de mesures permettant d'identifier un objet,
ou une partie d'un objet selon certains critères. Extraire plusieurs descripteurs
depuis une image peut donc permettre d'identifier cette image plus tard, sans avoir
besoin de recourir à l'image dans sa totalité. Il suffira pour cela d'utiliser
les descripteurs qu'on en aura extraits. De plus, les descripteurs peuvent
permettre de reconnaitre une image, même si elle est incomplète.

\paragraph{}
Les descripteurs \textsc{sift} disposent de propriétés intéressantes. Ils sont
invariants à la rotation et à l'échelle. C'est-à-dire que l'on peut détecter une
objet dans une image même si il a une taille ou une orientation différente de
l'original. Pour parvenir à ces résultats on sélectionne les points d'intérêt
de la manière suivante :

\subparagraph{\textit{Scale space}}
On construit d'abord ce que l'on appelle un \textit{Scale space}, qui correspond
à une même image, mais à des échelles et des niveaux de flou gaussiens différents.
\`A chaque échelle on applique différents niveaux de flou gaussiens à l'image
originale. On calcule ensuite des Laplaciens à chaque échelle en soustrayant des
images floutées entre elles afin d'extraire les contours. Cette étape est illustrée
par la figure [\ref{fig:dog}]. Dans chacun de ces Laplaciens on identifie les
extremum locaux, qui sont des points dont l'intensité est supérieure à celles de
ses voisins[\ref{fig:extrema}].

\begin{figure}[hbtp]
\centering
\includegraphics[width=0.6\textwidth]{sift_dog.jpg}
\caption{Scale space avec différence de gaussiennes}
\label{fig:dog}
\end{figure}

\begin{figure}[hbtp]
\centering
\includegraphics[width=0.5\textwidth]{sift_local_extrema.jpg}
\caption{Extremum locaux}
\label{fig:extrema}
\end{figure}

\subparagraph{Sélection des \textit{Keypoints}}
L'étape précédente peut avoir identifié un nombre très important de points d'intérêt,
dont la plupart n'apportent que peu d'information. L'étape suivante est donc de
sélectionner les points d'intérêts riches en information. Nous allons donc éliminer
les points de faible contraste (le seuil étant fixé arbitrairement), ainsi que
les points positionnés sur des bord. On garde ainsi les points sur les coins dont
le contraste est fort.

\subparagraph{invariance à la rotation}
\`A chaque point d'intérêt sont associées des information sur les gradients dans
toutes les directions afin d'assurer l'invariance à la rotation.
 


\subsubsection{Matching}

\paragraph{}
Des descripteurs \textsc{sift} sont extraits d'images de canettes de référence.
Ensuite, pour chaque objet que nous cherchons à identifier, on en extrait des
descripteurs et on va les comparer avec ceux de référence. Pour ce
faire, nous allons utiliser un \textsc{knn} afin d'estimer rapidement la distance
entre notre descripteur candidat et ses plus proches voisins de référence. Si la
distance est en dessous d'un seuil, on considère que l'on a reconnu un descripteur
de notre canette.


\subsection{\textit{Speeded Up Robust Features}}

\subsubsection{Principe}

\paragraph{}
Les descripteurs \textsc{surf} sont une amélioration de \textsc{sift} visant
principalement à améliorer les performances, tant pour le calcul des
descripteurs que pour la phase de matching. Pour cela, toutes les étapes de
\textsc{sift} ont été modifiées. Le Laplacien est approximé avec des
\textit{box filter}, ce qui permet l'utilisation d'image intégrale ainsi que le
calcul parallèle pour différentes échelles. L'invariance à la rotation est
assurée grâce à un calcul d'ondelettes horizontales et verticales dans un
voisinage. Le calcule d'ondelette peut être accéléré à toutes les échelles par
l'utilisation d'une image intégrale. Tout ceci permet d'assurer des performances
3 fois supérieures à \textsc{sift}, de plus, des implémentations sur \textsc{gpu}
existent.

\subsubsection{Matching}
Tout comme pour les descripteurs \textsc{sift}, le matching entre les descripteurs
de l'image de référence et les objets présents dans l'image a été effectué grâce à
une approche \textsc{knn}. Les résultats sont globalement équivalents à ce qu'on
peut obtenir avec \textsc{sift}, mais plus rapide.

\subsection{Bounding Box}

\paragraph{}
Une méthode simple et efficace si les canettes sont bien visibles et orientées horizontalement
ou verticalement, est de simplement calculer le ration entre la longueur et la hauteur de la
boite englobante. Cette technique, bien que très simple, permet de discriminer assez bien les
différents objets de l'image. La condition étant que l'étape de pré-traitement ait réussit
à correctement englober la canette dans une \textit{bounding box}.


\section{Conclusion}

\paragraph{}
Après avoir exploré plusieurs techniques et testé les résultats obtenus, nous avons décidé
de retenir la suite d'étapes suivantes pour la détection de canettes de coca-cola :
\begin{enumerate}
	\item passage en domaine \textsc{hsv}~;
	\item filtrage de la couleur rouge grâce à des seuils déterminées avec
	les histogrammes~;
	\item détection des contours~;
	\item création de \textit{bounding box} autour des contours~;
	\item détection des descripteurs \textsc{surf} sur chaque objet~;
	\item comparaison avec les descripteurs de référence grâce à un \textsc{knn}.
\end{enumerate}
Au final on obtient le résultat suivant [\ref{fig:result}]. Il est à noter que, même si
les résultats sont globalement bon, certains cas ne sont pas bien gérés. Par exemple, lorsque
la canette est trop petite ou qu'elle se situe sur un autre objet rouge, la détection échoue souvent.
Ceci est du au pré-traitement, qui rencontre deux problèmes :
\begin{enumerate}
	\item n'arrive pas à créer une \textit{bounding box} autour de la canette~;
	\item filtre trop les couleurs rouges pour que la canette soit en état d'être détectée.
\end{enumerate}


\begin{figure}[hbtp]
\centering
\includegraphics[width=0.8\textwidth]{result.jpg}
\caption{Image résultante après détection d'une canette}
\label{fig:result}
\end{figure}

\end{document}